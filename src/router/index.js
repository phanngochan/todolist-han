import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../page/HomeView'
import DogRandom from '../page/DogRandom'
import DogNameList from '../page/DogNameList'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path: '/dog/random',
    name: 'dograndom',
    component: DogRandom,

  },
  {
    path: '/dog/list',
    name: 'dognamelist',
    component: DogNameList
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
